﻿import unittest
import requests
#import base64
import json
from Card import Card
import os
import datetime

class Test_Export(unittest.TestCase):

    # tutaj należy zmienić port
    @property
    def harborUrl(self):
        return "http://localhost:57864/"
    
    def prepare_Data(self):
        listOfCards = {}

        movie = "Last.Vegas.2013.1080p.BluRay.x264.YIFY" 
        word = "graduate"
        translation = "kończyć studia" 
        quotes = ["...And they see how wrong it is and how unfortunate it is...", "...Perhaps it was the first that the unfortunate creature had ever shed..."]
        definitions = [("Complete a university degree", ["ukończyć studia", "ukończyć studia, zakończyć studia, koniec"]),
                        ("College graduate", ["absolwent wyższej uczelni"]), 
                        ("Holder of a university degree", ["abiturient"]),
                        ("Graduate education", ["wykształcenie podyplomowe"]), 
                        ("Relating to studies after BA", ["ostatniego roku"]), 
                        ("Graduate assistant", ["asystent", "asystentka"])]
        card = Card(movie, word, translation, quotes, definitions)
        dic = card.__dict__
        listOfCards[0] = dic

        movie = "Cosmos.A.Spacetime.Odyssey.S01E01" 
        word = "soluble"
        translation = "rozstrzygalny" 
        quotes = ["...Unlike the lead in paint, tetraethyl lead was fat soluble...", "...By providing methods of determining which problems are soluble, Kleene's work led to the study of which functions can be computed..."]
        definitions = [("(of a substance) capable of being dissolved in some solvent (usually water)", ["rozstrzygalny", "rozstrzygalna"]),
                        ("susceptible of solution or of being solved or explained", ["rozstrzygalny", "rozstrzygalna"]),
                        ("(of a problem) able to be solved", ["rozwiązalny", "rozwiązywalny"])]
        card = Card(movie, word, translation, quotes, definitions)
        dic = card.__dict__
        listOfCards[1] = dic

        movie = "Cosmos.A.Spacetime.Odyssey.S01E01" 
        word = "quantify"
        translation = "odmierzać" 
        quotes = ["...so that the amounts of each element can be quantified...", "...These measures enabled researchers to quantify each individual act of violence in each film..."]
        definitions = [("express or measure the quantity of.", ["odmierzać", "taksować", "określić ilościowo"]),
                        ("define the application of (a term or proposition) by the use of all, some", ["odmierzać"])]
        card = Card(movie, word, translation, quotes, definitions)
        dic = card.__dict__
        listOfCards[2] = dic

        movie = "Cosmos.A.Spacetime.Odyssey.S01E01" 
        word = "own"
        translation = "własny" 
        quotes = ["...Vested interests still hire their own scientists...", "...Nature, Patterson put his own name second..."]
        definitions = [("have", ["posiadać", "mieć"]),
                        ("possessed by", ["własny"]),
                        ("at your own discretion", ["do twojej dyspozycji"]),
                        ("from my own experience", ["z mojego własnego doświadczenia"]),
                        ("own", ["rodzony"]),
                        ("admit or acknowledge that something is the case or that one feels a certain way", ["własny"])]
        card = Card(movie, word, translation, quotes, definitions)
        dic = card.__dict__
        listOfCards[3] = dic

        movie = "Cosmos.A.Spacetime.Odyssey.S01E01" 
        word = "slight"
        translation = "drobny" 
        quotes = ["...Not the slightest...", "...There is not the slightest evidence..."]
        definitions = [("very little", ["drobny"]),
                        ("body: thin", ["smukły"]),
                        ("slight bend", ["łagodny zakręt"]),
                        ("slight improvement", ["lekka poprawa"]),
                        ("thin", ["cienki", "wątły"])]
        card = Card(movie, word, translation, quotes, definitions)
        dic = card.__dict__
        listOfCards[4] = dic

        movie = "Cosmos.A.Spacetime.Odyssey.S01E01" 
        word = "acid"
        translation = "kwas" 
        quotes = ["...Tools in acid and purify all his chemicals...", "...‘Big surprise there,’ though with considerably less acid in his voice..."]
        definitions = [("chemical compound", ["kwas"]),
                        ("drugs: LSD", ["kwas"]),
                        ("food: sharp in taste", ["kwaśny"]),
                        ("acid rain", ["kwaśne deszcze"]),
                        ("snappish", ["zgryźliwy"])]
        card = Card(movie, word, translation, quotes, definitions)
        dic = card.__dict__
        listOfCards[5] = dic

        movie = "Cosmos.A.Spacetime.Odyssey.S01E01" 
        word = "establish"
        translation = "założyć" 
        quotes = ["...One whose age had already been established...", "...We must now establish who bought the estate..."]
        definitions = [("create", ["zakładać", "założyć"]),
                        ("install", ["ustanawiać", "ustanowić"]),
                        ("establish a connection", ["kontaktować się", "skontaktować się"])]
        card = Card(movie, word, translation, quotes, definitions)
        dic = card.__dict__
        listOfCards[6] = dic

        movie = "Cosmos.A.Spacetime.Odyssey.S01E01" 
        word = "fulfill"
        translation = "spełniać" 
        quotes = ["...That can't fulfill the cell's vital needs...", "...Women are expected to fulfill the traditional role of homemaker..."]
        definitions = [("obligaion: meet", ["spełniać", "spełnić"]),
                        ("promise: keep", ["spełniać", "realizować"]),
                        ("need: fill, satisfy", ["zaspokajać"]),
                        ("realize", ["urzeczywistniać"]),
                        ("satisfy", ["zadośćuczynić"])]
        card = Card(movie, word, translation, quotes, definitions)
        dic = card.__dict__
        listOfCards[7] = dic

        movie = "Cosmos.A.Spacetime.Odyssey.S01E01" 
        word = "intact"
        translation = "nienaruszony" 
        quotes = ["...No part of the earth's surface could survive intact...", "...The main mast, which is lying across the front deck, had its cylindrical shape intact..."]
        definitions = [("whole, unbroken", ["nienaruszony", "nietknięty"]),
                        ("keep intact", ["pilnować"]),
                        ("keep intact", ["nie naruszać"]),
                        ("whole", ["cały"]),
                        ("pristine, primeval", ["dziewiczy"])]
        card = Card(movie, word, translation, quotes, definitions)
        dic = card.__dict__
        listOfCards[8] = dic

        movie = "Cosmos.A.Spacetime.Odyssey.S01E01" 
        word = "operational"
        translation = "działający" 
        quotes = ["...It uses advanced technology to both deal with incoming telephone calls and to pass messages on to operational police..."]
        definitions = [("functioning, working", ["działający", "czynny"]),
                        ("operational research, US: operations research", ["inżynieria matematyczna"]),
                        ("combat, fighting", ["bojowy"])]
        card = Card(movie, word, translation, quotes, definitions)
        dic = card.__dict__
        listOfCards[9] = dic

        movie = "Cosmos.A.Spacetime.Odyssey.S01E01" 
        word = "mortar"
        translation = "zaprawa" 
        quotes = ["...I know a place where the unused bricks and mortar left over...", "...Individual garnets were cut from selected samples, crushed in a mortar and pestle and sieved..."]
        definitions = [("for bricks, tabby", ["zaprawa"]),
                        ("grinding bowl", ["moździerz"]),
                        ("weapon", ["moździerz"]),
                        ("verb", ["wiązać zaprawą"])]
        card = Card(movie, word, translation, quotes, definitions)
        dic = card.__dict__
        listOfCards[10] = dic

        movie = "Cosmos.A.Spacetime.Odyssey.S01E01" 
        word = "manufacturer"
        translation = "producent" 
        quotes = ["...He has a specially constructed recording studio in his back yard and works these days as a record manufacturer and a musician..."]
        definitions = [("business", ["producent"]),
                        ("cement manufacturer", ["producent cementu"]),
                        ("soft drinks manufacturer", ["firma produkująca napoje bezalkoholowe"]),
                        ("producer", ["wytwórca", "producent"]),
                        ("crafsman", ["rękodzielnik"])]
        card = Card(movie, word, translation, quotes, definitions)
        dic = card.__dict__
        listOfCards[11] = dic

        movie = "Cosmos.A.Spacetime.Odyssey.S01E01" 
        word = "masquerade"
        translation = "maskarada" 
        quotes = ["...Enzymes in the cell are fooled by the lead's masquerade...", "...It is unlikely we will soon return to a masquerade of can-can supported grandeur and today's parade music must bow to that reality-not the other way around..."]
        definitions = [("masked ball", ["bal maskowy", "maskarada"]),
                        ("pretended to be", ["przebierać się", "przebrać się"]),
                        ("verb", ["wystąpić w przebraniu"]),
                        ("mummery", ["pantomima"])]
        card = Card(movie, word, translation, quotes, definitions)
        dic = card.__dict__
        listOfCards[12] = dic

        movie = "Cosmos.A.Spacetime.Odyssey.S01E01" 
        word = "occupational"
        translation = "zawodowy" 
        quotes = ["...Yes, there might be occupational hazards...", "...The scheme also involved bringing in an occupational health nurse to give on-the-spot advice..."]
        definitions = [("of or caused by one's work", ["zawodowy"]),
                        ("occupational hazard", ["ryzyko zawodowe"]),
                        ("occupational therapy", ["terapia zajęciowa"]),
                        ("professed", ["zawodowy", "zajęciowy"])]
        card = Card(movie, word, translation, quotes, definitions)
        dic = card.__dict__
        listOfCards[13] = dic

        return listOfCards

    def test_Export(self):
       
        listOfCards = {}

        movie = "Last.Vegas.2013.1080p.BluRay.x264.YIFY" 
        word = "graduate"
        translation = "kończyć studia" 
        quotes = ["...And they see how wrong it is and how unfortunate it is...", "...Perhaps it was the first that the unfortunate creature had ever shed..."]
        definitions = [("Complete a university degree", ["ukończyć studia", "ukończyć studia, zakończyć studia, koniec"]),
                        ("College graduate", ["absolwent wyższej uczelni"]), 
                        ("Holder of a university degree", ["abiturient"]),
                        ("Graduate education", ["wykształcenie podyplomowe"]), 
                        ("Relating to studies after BA", ["ostatniego roku"]), 
                        ("Graduate assistant", ["asystent", "asystentka"])]
        card = Card(movie, word, translation, quotes, definitions)
        dic = card.__dict__
        listOfCards[0] = dic

        movie = "Cosmos.A.Spacetime.Odyssey.S01E01" 
        word = "soluble"
        translation = "rozstrzygalny" 
        quotes = ["...Unlike the lead in paint, tetraethyl lead was fat soluble...", "...By providing methods of determining which problems are soluble, Kleene's work led to the study of which functions can be computed..."]
        definitions = [("(of a substance) capable of being dissolved in some solvent (usually water)", ["rozstrzygalny", "rozstrzygalna"]),
                        ("susceptible of solution or of being solved or explained", ["rozstrzygalny", "rozstrzygalna"]),
                        ("(of a problem) able to be solved", ["rozwiązalny", "rozwiązywalny"])]
        card = Card(movie, word, translation, quotes, definitions)
        dic = card.__dict__
        listOfCards[1] = dic

        movie = "Cosmos.A.Spacetime.Odyssey.S01E01" 
        word = "quantify"
        translation = "odmierzać" 
        quotes = ["...so that the amounts of each element can be quantified...", "...These measures enabled researchers to quantify each individual act of violence in each film..."]
        definitions = [("express or measure the quantity of.", ["odmierzać", "taksować", "określić ilościowo"]),
                        ("define the application of (a term or proposition) by the use of all, some", ["odmierzać"])]
        card = Card(movie, word, translation, quotes, definitions)
        dic = card.__dict__
        listOfCards[2] = dic

        movie = "Cosmos.A.Spacetime.Odyssey.S01E01" 
        word = "own"
        translation = "własny" 
        quotes = ["...Vested interests still hire their own scientists...", "...Nature, Patterson put his own name second..."]
        definitions = [("have", ["posiadać", "mieć"]),
                        ("possessed by", ["własny"]),
                        ("at your own discretion", ["do twojej dyspozycji"]),
                        ("from my own experience", ["z mojego własnego doświadczenia"]),
                        ("own", ["rodzony"]),
                        ("admit or acknowledge that something is the case or that one feels a certain way", ["własny"])]
        card = Card(movie, word, translation, quotes, definitions)
        dic = card.__dict__
        listOfCards[3] = dic

        movie = "Cosmos.A.Spacetime.Odyssey.S01E01" 
        word = "slight"
        translation = "drobny" 
        quotes = ["...Not the slightest...", "...There is not the slightest evidence..."]
        definitions = [("very little", ["drobny"]),
                        ("body: thin", ["smukły"]),
                        ("slight bend", ["łagodny zakręt"]),
                        ("slight improvement", ["lekka poprawa"]),
                        ("thin", ["cienki", "wątły"])]
        card = Card(movie, word, translation, quotes, definitions)
        dic = card.__dict__
        listOfCards[4] = dic

        movie = "Cosmos.A.Spacetime.Odyssey.S01E01" 
        word = "acid"
        translation = "kwas" 
        quotes = ["...Tools in acid and purify all his chemicals...", "...‘Big surprise there,’ though with considerably less acid in his voice..."]
        definitions = [("chemical compound", ["kwas"]),
                        ("drugs: LSD", ["kwas"]),
                        ("food: sharp in taste", ["kwaśny"]),
                        ("acid rain", ["kwaśne deszcze"]),
                        ("snappish", ["zgryźliwy"])]
        card = Card(movie, word, translation, quotes, definitions)
        dic = card.__dict__
        listOfCards[5] = dic

        movie = "Cosmos.A.Spacetime.Odyssey.S01E01" 
        word = "establish"
        translation = "założyć" 
        quotes = ["...One whose age had already been established...", "...We must now establish who bought the estate..."]
        definitions = [("create", ["zakładać", "założyć"]),
                        ("install", ["ustanawiać", "ustanowić"]),
                        ("establish a connection", ["kontaktować się", "skontaktować się"])]
        card = Card(movie, word, translation, quotes, definitions)
        dic = card.__dict__
        listOfCards[6] = dic

        movie = "Cosmos.A.Spacetime.Odyssey.S01E01" 
        word = "fulfill"
        translation = "spełniać" 
        quotes = ["...That can't fulfill the cell's vital needs...", "...Women are expected to fulfill the traditional role of homemaker..."]
        definitions = [("obligaion: meet", ["spełniać", "spełnić"]),
                        ("promise: keep", ["spełniać", "realizować"]),
                        ("need: fill, satisfy", ["zaspokajać"]),
                        ("realize", ["urzeczywistniać"]),
                        ("satisfy", ["zadośćuczynić"])]
        card = Card(movie, word, translation, quotes, definitions)
        dic = card.__dict__
        listOfCards[7] = dic

        movie = "Cosmos.A.Spacetime.Odyssey.S01E01" 
        word = "intact"
        translation = "nienaruszony" 
        quotes = ["...No part of the earth's surface could survive intact...", "...The main mast, which is lying across the front deck, had its cylindrical shape intact..."]
        definitions = [("whole, unbroken", ["nienaruszony", "nietknięty"]),
                        ("keep intact", ["pilnować"]),
                        ("keep intact", ["nie naruszać"]),
                        ("whole", ["cały"]),
                        ("pristine, primeval", ["dziewiczy"])]
        card = Card(movie, word, translation, quotes, definitions)
        dic = card.__dict__
        listOfCards[8] = dic

        movie = "Cosmos.A.Spacetime.Odyssey.S01E01" 
        word = "operational"
        translation = "działający" 
        quotes = ["...It uses advanced technology to both deal with incoming telephone calls and to pass messages on to operational police..."]
        definitions = [("functioning, working", ["działający", "czynny"]),
                        ("operational research, US: operations research", ["inżynieria matematyczna"]),
                        ("combat, fighting", ["bojowy"])]
        card = Card(movie, word, translation, quotes, definitions)
        dic = card.__dict__
        listOfCards[9] = dic

        movie = "Cosmos.A.Spacetime.Odyssey.S01E01" 
        word = "mortar"
        translation = "zaprawa" 
        quotes = ["...I know a place where the unused bricks and mortar left over...", "...Individual garnets were cut from selected samples, crushed in a mortar and pestle and sieved..."]
        definitions = [("for bricks, tabby", ["zaprawa"]),
                        ("grinding bowl", ["moździerz"]),
                        ("weapon", ["moździerz"]),
                        ("verb", ["wiązać zaprawą"])]
        card = Card(movie, word, translation, quotes, definitions)
        dic = card.__dict__
        listOfCards[10] = dic

        movie = "Cosmos.A.Spacetime.Odyssey.S01E01" 
        word = "manufacturer"
        translation = "producent" 
        quotes = ["...He has a specially constructed recording studio in his back yard and works these days as a record manufacturer and a musician..."]
        definitions = [("business", ["producent"]),
                        ("cement manufacturer", ["producent cementu"]),
                        ("soft drinks manufacturer", ["firma produkująca napoje bezalkoholowe"]),
                        ("producer", ["wytwórca", "producent"]),
                        ("crafsman", ["rękodzielnik"])]
        card = Card(movie, word, translation, quotes, definitions)
        dic = card.__dict__
        listOfCards[11] = dic

        movie = "Cosmos.A.Spacetime.Odyssey.S01E01" 
        word = "masquerade"
        translation = "maskarada" 
        quotes = ["...Enzymes in the cell are fooled by the lead's masquerade...", "...It is unlikely we will soon return to a masquerade of can-can supported grandeur and today's parade music must bow to that reality-not the other way around..."]
        definitions = [("masked ball", ["bal maskowy", "maskarada"]),
                        ("pretended to be", ["przebierać się", "przebrać się"]),
                        ("verb", ["wystąpić w przebraniu"]),
                        ("mummery", ["pantomima"])]
        card = Card(movie, word, translation, quotes, definitions)
        dic = card.__dict__
        listOfCards[12] = dic

        movie = "Cosmos.A.Spacetime.Odyssey.S01E01" 
        word = "occupational"
        translation = "zawodowy" 
        quotes = ["...Yes, there might be occupational hazards...", "...The scheme also involved bringing in an occupational health nurse to give on-the-spot advice..."]
        definitions = [("of or caused by one's work", ["zawodowy"]),
                        ("occupational hazard", ["ryzyko zawodowe"]),
                        ("occupational therapy", ["terapia zajęciowa"]),
                        ("professed", ["zawodowy", "zajęciowy"])]
        card = Card(movie, word, translation, quotes, definitions)
        dic = card.__dict__
        listOfCards[13] = dic

        headers = {'content-type': 'application/json; charset=utf-8'}
        r = requests.post(self.harborUrl + "ExportToPdf", data=json.dumps(listOfCards), headers=headers)
        parseResponse = r.content

        # sprawdzenie odebranych danych - zapisanie do pliku
        filename = 'generatedPDF' + datetime.datetime.now().strftime("%Y-%m-%d-%H-%M-%S") + '.pdf'
        with open(os.path.join(os.path.dirname(__file__), filename), 'wb', -1) as temp_file:
            temp_file.write(parseResponse)

        def test_Metadata(self):
            # Metadata should return some basic info about what whole harbor
            # does
            r = requests.get(self.harborUrl + "Metadata")
            metadata = r.json()


            self.assertEqual(metadata['Movie'], "Title of movie")



if __name__ == '__main__':
    unittest.main()
