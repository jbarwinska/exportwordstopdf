class Card(object):
    movie = ""
    word = ""
    translation = ""
    quotes = []
    definitions = []

    def __init__(self, movie, word, translation, quotes=None, definitions=None):
        if quotes is None:
            self.quotes = []
        if definitions is None:
            self.definitions = []

        self.movie = movie
        self.word = word
        self.translation = translation
        self.quotes = quotes
        self.definitions = definitions


  