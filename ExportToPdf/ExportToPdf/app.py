﻿from flask import Flask, jsonify, request
app = Flask(__name__)

# Make the WSGI interface available at the top level so wfastcgi can get it.
wsgi_app = app.wsgi_app

from Card import Card
import os
import subprocess
import jinja2
import datetime
import json

# sparsowanie jsona
def getJson(request):
    cards = []
    list = request.json
    count = len(list.items())
    for i in range(count):
        card = list[str(i)]
        cards.append(card)
    return cards

def createHtmlTemplate(cardList):
    templateEnv = jinja2.Environment(autoescape=True,
    loader=jinja2.FileSystemLoader(os.path.join(os.path.dirname(__file__), 'templates')))
 
    # wczytanie szablonu
    template = templateEnv.get_template('template.html')

    # stworzenie odpowiedniego słownika dla templatu
    my_cards = {}
    index = 0
    size = len(cardList)
    for i in range(size):
        if i != 0 and i % 12 == 0:
            index = index + 1  
        column = i % 12
        c = cardList[i]
        if column == 0:
            my_cards[index] = { column: c}
        else:
            my_cards[index].update({ column: c}) 
   
    templateVars = { "cards": my_cards }

    # generowanie htmla
    outputText = template.render(templateVars)
    return outputText

def saveRendererHtml(rendererTemplate):
     #zapis do pliku
    filename = 'rendererTemplate.html'
    with open(os.path.join(os.path.dirname(__file__), filename), 'w', -1, "UTF-8") as temp_file:
        temp_file.write(rendererTemplate)

def render_to_pdf():
    pdfPath = 'generate' + datetime.datetime.now().strftime("%Y-%m-%d-%H-%M-%S") + '.pdf'
    subprocess.call([os.path.join(os.path.dirname(__file__), 'wkhtmltopdf\\bin\\wkhtmltopdf.exe'), 
                     os.path.join(os.path.dirname(__file__), 'rendererTemplate.html'), 
                     os.path.join(os.path.dirname(__file__), pdfPath)])

    return open(os.path.join(os.path.dirname(__file__), pdfPath), "rb").read() 



@app.route('/ExportToPdf', methods=['POST'])
def export():
    if not request.json:
        abort(400)

    listOfCards = getJson(request)
    rendererTemplate = createHtmlTemplate(listOfCards)
    saveRendererHtml(rendererTemplate)
    bytes = render_to_pdf()
    return bytes




if __name__ == '__main__':
    import os
    HOST = os.environ.get('SERVER_HOST', 'localhost')
    try:
        PORT = int(os.environ.get('SERVER_PORT', '6666'))
    except ValueError:
        PORT = 6666
    app.run(HOST, PORT)
