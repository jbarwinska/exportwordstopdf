�}q (X   docqX  This module provides various functions to manipulate time values.

There are two standard representations of time.  One is the number
of seconds since the Epoch, in UTC (a.k.a. GMT).  It may be an integer
or a floating point number (to represent fractions of seconds).
The Epoch is system-defined; on Unix, it is generally January 1st, 1970.
The actual value can be retrieved by calling gmtime(0).

The other representation is a tuple of 9 integers giving local time.
The tuple items are:
  year (including century, e.g. 1998)
  month (1-12)
  day (1-31)
  hours (0-23)
  minutes (0-59)
  seconds (0-59)
  weekday (0-6, Monday is 0)
  Julian day (day in the year, 1-366)
  DST (Daylight Savings Time) flag (-1, 0 or 1)
If the DST flag is 0, the time is given in the regular time zone;
if it is 1, the time is given in the DST time zone;
if it is -1, mktime() should guess based on the date and time.

Variables:

timezone -- difference in seconds between UTC and local standard time
altzone -- difference in  seconds between UTC and local DST time
daylight -- whether local time should reflect DST
tzname -- tuple of (standard time zone name, DST time zone name)

Functions:

time() -- return current time in seconds since the Epoch as a float
clock() -- return CPU time since process start as a float
sleep() -- delay for a number of seconds given as a float
gmtime() -- convert seconds since Epoch to UTC tuple
localtime() -- convert seconds since Epoch to local time tuple
asctime() -- convert time tuple to string
ctime() -- convert time in seconds to string
mktime() -- convert local time tuple to seconds since Epoch
strftime() -- convert time tuple to string according to format specification
strptime() -- parse string to time tuple according to format specification
tzset() -- change the local timezoneqX   membersq}q(X   __name__q}q(X   kindqX   dataqX   valueq	}q
X   typeq]q(X   builtinsqX   strq�qX   __builtin__qX   strq�qesuX   _STRUCT_TM_ITEMSq}q(hhh	}qh]qhX   intq�qasuX
   __loader__q}q(hX   typerefqh	]qX   _frozen_importlibqX   BuiltinImporterq�qauX   __package__q }q!(hhh	}q"h]q#(hhX   NoneTypeq$�q%esuX   perf_counterq&}q'(hX   functionq(h	}q)(X	   overloadsq*]q+}q,(X   argsq-)hX%   Performance counter for benchmarking.q.X   ret_typeq/]q0hX   floatq1�q2auahX>   perf_counter() -> float

Performance counter for benchmarking.q3uuX   struct_timeq4}q5(hhh	}q6(h}q7(X   tm_hourq8}q9(hX   propertyq:h	}q;(hX   hours, range [0, 23]q<h]q=(hX   objectq>�q?hX   objectq@�qAeuuX   tm_secqB}qC(hh:h	}qD(hX   seconds, range [0, 61])qEh]qF(h?hAeuuX   __delattr__qG}qH(hX   methodqIh	}qJ(h*]qK}qL(X   argsqM}qN(X   nameqOX   selfqPX   typeqQ]qRhAau}qS(hOX   nameqThQ]qUhau�qVX   ret_typeqW]qXh%auahX   Implement delattr(self, name).qYuuX
   __format__qZ}q[(hhIh	}q\(h*]q]}q^(hM}q_(hOX   selfq`hQ]qahAau}qb(hOX
   formatSpecqchQ]qdhau�qehW]qfhauahX   default object formatterqguuX   __hash__qh}qi(hhIh	}qj(h*]qkhX   Return hash(self).qluuX   __repr__qm}qn(hhIh	}qo(h*]qp}qq(hM}qr(hOX   selfqshQ]qtX   timequX   struct_timeqv�qwau�qxhW]qyhauahX   Return repr(self).qzuuX   __lt__q{}q|(hhIh	}q}(h*]q~(}q(hM}q�(hOX   yq�hQ]q�hAau}q�(hOX   xq�hQ]q�hwau�q�hW]q�hAau}q�(hM}q�(hOh�hQ]q�hwau}q�(hOh�hQ]q�hAau�q�hW]q�hAau}q�(hM}q�(hOh�hQ]q�hwau}q�(hOh�hQ]q�hwau�q�hW]q�hX   boolq��q�auehX   Return self<value.q�uuX   tm_yearq�}q�(hh:h	}q�(hX   year, for example, 1993q�h]q�(h?hAeuuX   __add__q�}q�(hhIh	}q�(h*]q�}q�(hM}q�(hOh�hQ]q�hX   tupleq��q�au}q�(hOh�hQ]q�h�au�q�hW]q�h�auahX   Return self+value.q�uuX   __gt__q�}q�(hhIh	}q�(h*]q�(}q�(hM}q�(hOh�hQ]q�hAau}q�(hOh�hQ]q�hwau�q�hW]q�hAau}q�(hM}q�(hOh�hQ]q�hwau}q�(hOh�hQ]q�hAau�q�hW]q�hAau}q�(hM}q�(hOh�hQ]q�hwau}q�(hOh�hQ]q�hwau�q�hW]q�h�auehX   Return self>value.q�uuX   __str__q�}q�(hhIh	}q�(h*]q�}q�(hM}q�(hOX   selfq�hQ]q�hAau�q�hW]q�hauahX   Return str(self).q�uuX   __dir__q�}q�(hhIh	}q�(h*]q�}q�(h-}q�(X   nameq�X   selfq�h]q�hX   objectqچq�au�q�hX   default dir() implementationq�h/]q�hX   listq߆q�auahX.   __dir__() -> list
default dir() implementationq�uuX
   __reduce__q�}q�(hhIh	}q�(h*]q�}q�(hM}q�(hOhshQ]q�hwau�q�hW]q�h�auahX   helper for pickleq�uuX   tm_mdayq�}q�(hh:h	}q�(hX   day of month, range [1, 31]q�h]q�(h?hAeuuX	   __class__q�}q�(hhh	]q�hX   typeq�q�auX   __reduce_ex__q�}q�(hhIh	}q�(h*]q�(}q�(hM}q�(hOX   selfq�hQ]q�hAau}q�(hOX   protocolq�hQ]r   hAau�r  hW]r  hAau}r  (hM}r  (hOX   selfr  hQ]r  hAau�r  hW]r  hAauehX   helper for pickler	  uuX   __eq__r
  }r  (hhIh	}r  (h*]r  (}r  (hM}r  (hOh�hQ]r  hAau}r  (hOh�hQ]r  hwau�r  hW]r  hAau}r  (hM}r  (hOh�hQ]r  hwau}r  (hOh�hQ]r  hAau�r  hW]r  hAau}r  (hM}r  (hOh�hQ]r  hwau}r  (hOh�hQ]r   hwau�r!  hW]r"  h�auehX   Return self==value.r#  uuX   __subclasshook__r$  }r%  (hh(h	}r&  (h*NhX4  Abstract classes can override this to customize issubclass().

This is invoked early on by abc.ABCMeta.__subclasscheck__().
It should return True, False or NotImplemented.  If it returns
NotImplemented, the normal algorithm is used.  Otherwise, it
overrides the normal algorithm (and the outcome is cached).
r'  uuX   tm_wdayr(  }r)  (hh:h	}r*  (hX&   day of week, range [0, 6], Monday is 0r+  h]r,  (h?hAeuuX   __iter__r-  }r.  (hhIh	}r/  (h*]r0  hX   Implement iter(self).r1  uuX   __setattr__r2  }r3  (hhIh	}r4  (h*]r5  }r6  (hM}r7  (hOX   selfr8  hQ]r9  hAau}r:  (hOX   namer;  hQ]r<  hau}r=  (hOX   valuer>  hQ]r?  hAau�r@  hW]rA  h%auahX%   Implement setattr(self, name, value).rB  uuX   __ge__rC  }rD  (hhIh	}rE  (h*]rF  (}rG  (hM}rH  (hOh�hQ]rI  hAau}rJ  (hOh�hQ]rK  hwau�rL  hW]rM  hAau}rN  (hM}rO  (hOh�hQ]rP  hwau}rQ  (hOh�hQ]rR  hAau�rS  hW]rT  hAau}rU  (hM}rV  (hOh�hQ]rW  hwau}rX  (hOh�hQ]rY  hwau�rZ  hW]r[  h�auehX   Return self>=value.r\  uuX   __init__r]  }r^  (hhIh	}r_  (h*]r`  (}ra  (hM}rb  (hOX   selfrc  hQ]rd  hAau}re  (X
   arg_formatrf  X   **rg  hOX   kwargsrh  hQ]ri  hX   dictrj  �rk  au}rl  (jf  X   *rm  hOX   argsrn  hQ]ro  h�au�rp  hW]rq  h%au}rr  (hM}rs  (hOX   selfrt  hQ]ru  hAau}rv  (jf  jm  hOX   argsrw  hQ]rx  h�au�ry  hW]rz  h%au}r{  (hM}r|  (hOX   selfr}  hQ]r~  hAau�r  hW]r�  h%auehX>   Initialize self.  See help(type(self)) for accurate signature.r�  uuX   __getitem__r�  }r�  (hhIh	}r�  (h*]r�  (}r�  (hM}r�  (hOhshQ]r�  h�au}r�  (hOX   indexr�  hQ]r�  hX   longr�  �r�  au�r�  hW]r�  hAau}r�  (hM}r�  (hOhshQ]r�  h�au}r�  (hOX   slicer�  hQ]r�  hX   slicer�  �r�  au�r�  hW]r�  hAau}r�  (hM}r�  (hOhshQ]r�  h�au}r�  (hOX   indexr�  hQ]r�  hX   intr�  �r�  au�r�  hW]r�  hAau}r�  (hM}r�  (hOhshQ]r�  h�au}r�  (hOX   indexr�  hQ]r�  hAau�r�  hW]r�  hAauehX   Return self[key].r�  uuX   tm_minr�  }r�  (hh:h	}r�  (hX   minutes, range [0, 59]r�  h]r�  (h?hAeuuX   __getnewargs__r�  }r�  (hhIh	}r�  h*NsuX   tm_monr�  }r�  (hh:h	}r�  (hX   month of year, range [1, 12]r�  h]r�  (h?hAeuuX   __rmul__r�  }r�  (hhIh	}r�  (h*]r�  (}r�  (hM}r�  (hOX   countr�  hQ]r�  hAau}r�  (hOX   selfr�  hQ]r�  h�au�r�  hW]r�  hAau}r�  (hM}r�  (hOX   nr�  hQ]r�  j�  au}r�  (hOh�hQ]r�  h�au�r�  hW]r�  h�auehX   Return self*value.r�  uuX   n_fieldsr�  }r�  (hhh	}r�  h]r�  hasuX   __le__r�  }r�  (hhIh	}r�  (h*]r�  (}r�  (hM}r�  (hOh�hQ]r�  hAau}r�  (hOh�hQ]r�  hwau�r�  hW]r�  hAau}r�  (hM}r�  (hOh�hQ]r�  hwau}r�  (hOh�hQ]r�  hAau�r�  hW]r�  hAau}r�  (hM}r�  (hOh�hQ]r�  hwau}r�  (hOh�hQ]r�  hwau�r�  hW]r�  h�auehX   Return self<=value.r�  uuX   n_sequence_fieldsr�  }r�  (hhh	}r�  h]r�  hasuX   indexr�  }r�  (hhIh	}r�  (h*]r�  (}r�  (h-(}r�  (h�h�h]r�  h�au}r�  h�X   valuer�  s}r�  (X   default_valuer�  X   Noner�  h�X   startr�  u}r�  (j�  j�  h�X   stopr   utr  hX0   .
Raises ValueError if the value is not present.r  h/]r  hX   intr  �r  au}r  (hM(}r  (hOhshQ]r  h�au}r	  (hOX   objr
  hQ]r  hAau}r  (hOX   startr  hQ]r  hAau}r  (hOX   endr  hQ]r  hAautr  hW]r  j�  au}r  (hM(}r  (hOhshQ]r  h�au}r  (hOX   objr  hQ]r  hAau}r  (hOX   startr  hQ]r  j�  au}r  (hOX   endr  hQ]r  j�  autr   hW]r!  j�  au}r"  (hM}r#  (hOhshQ]r$  h�au}r%  (hOX   objr&  hQ]r'  hAau}r(  (hOX   startr)  hQ]r*  hAau�r+  hW]r,  j�  au}r-  (hM}r.  (hOhshQ]r/  h�au}r0  (hOX   objr1  hQ]r2  hAau}r3  (X   default_valuer4  X   0r5  hOX   startr6  hQ]r7  j�  au�r8  hW]r9  j�  auehXy   T.index(value, [start, [stop]]) -> integer -- return first index of value.
Raises ValueError if the value is not present.r:  uuX   countr;  }r<  (hhIh	}r=  (h*]r>  (}r?  (h-}r@  (h�h�h]rA  h�au}rB  h�X   valuerC  s�rD  hX    rE  h/]rF  j  au}rG  (hM}rH  (hOhshQ]rI  h�au}rJ  (hOX   objrK  hQ]rL  hAau�rM  hW]rN  j�  auehXB   T.count(value) -> integer -- return number of occurrences of valuerO  uuX   __len__rP  }rQ  (hhIh	}rR  (h*]rS  }rT  (hM}rU  (hOhshQ]rV  h�au�rW  hW]rX  j�  auahX   Return len(self).rY  uuX   tm_ydayrZ  }r[  (hh:h	}r\  (hX   day of year, range [1, 366]r]  h]r^  (h?hAeuuX   __contains__r_  }r`  (hhIh	}ra  (h*]rb  hX   Return key in self.rc  uuX   __doc__rd  }re  (hhh	}rf  h]rg  (hhesuX   __new__rh  }ri  (hh(h	}rj  (h*]rk  (}rl  (hM}rm  (hOX   clsrn  hQ]ro  hX   typerp  �rq  au}rr  (hOX   sequencers  hQ]rt  h�au�ru  hW]rv  hwau}rw  (hM(}rx  (hOX   clsry  hQ]rz  jq  au}r{  (hOX   yearr|  hQ]r}  j�  au}r~  (hOX   monthr  hQ]r�  j�  au}r�  (hOX   dayr�  hQ]r�  j�  au}r�  (hOX   hourr�  hQ]r�  j�  au}r�  (hOX   minuter�  hQ]r�  j�  au}r�  (hOX   secondr�  hQ]r�  j�  au}r�  (hOX	   dayOfWeekr�  hQ]r�  j�  au}r�  (hOX	   dayOfYearr�  hQ]r�  j�  au}r�  (hOX   isDstr�  hQ]r�  j�  autr�  hW]r�  hwauehXG   Create and return a new object.  See help(type) for accurate signature.r�  uuX   __ne__r�  }r�  (hhIh	}r�  (h*]r�  (}r�  (hM}r�  (hOh�hQ]r�  hAau}r�  (hOh�hQ]r�  hwau�r�  hW]r�  hAau}r�  (hM}r�  (hOh�hQ]r�  hwau}r�  (hOh�hQ]r�  hAau�r�  hW]r�  hAau}r�  (hM}r�  (hOh�hQ]r�  hwau}r�  (hOh�hQ]r�  hwau�r�  hW]r�  h�auehX   Return self!=value.r�  uuX   tm_isdstr�  }r�  (hh:h	}r�  (hX:   1 if summer time is in effect, 0 if not, and -1 if unknownr�  h]r�  (h?hAeuuX
   __sizeof__r�  }r�  (hhIh	}r�  (h*]r�  (}r�  (h-}r�  (h�h�h]r�  h�au�r�  hX   size of T in memory, in bytesr�  u}r�  (hM}r�  (hOX   selfr�  hQ]r�  hAau�r�  hW]r�  j�  auehX/   T.__sizeof__() -- size of T in memory, in bytesr�  uuX   n_unnamed_fieldsr�  }r�  (hhh	}r�  h]r�  hasuX   __mul__r�  }r�  (hhIh	}r�  (h*]r�  (}r�  (hM}r�  (hOX   selfr�  hQ]r�  h�au}r�  (hOX   countr�  hQ]r�  hAau�r�  hW]r�  hAau}r�  (hM}r�  (hOh�hQ]r�  h�au}r�  (hOj�  hQ]r�  j�  au�r�  hW]r�  h�auehX   Return self*value.nr�  uuuhX�  The time value as returned by gmtime(), localtime(), and strptime(), and
 accepted by asctime(), mktime() and strftime().  May be considered as a
 sequence of 9 integers.

 Note that several fields' values are not the same as those defined by
 the C language standard for struct tm.  For example, the value of the
 field tm_year is the actual year, not year - 1900.  See individual
 fields' descriptions for details.r�  X   mror�  ]r�  (X   timer�  X   struct_timer�  �r�  hX   tupler�  �r�  h?eX   basesr�  ]r�  j�  auuX	   monotonicr�  }r�  (hh(h	}r�  (h*]r�  }r�  (h-)hX$   Monotonic clock, cannot go backward.r�  h/]r�  h2auahX:   monotonic() -> float

Monotonic clock, cannot go backward.r�  uuX   tznamer�  }r�  (hhh	}r�  h]r�  (j�  h�esuX   ctimer�  }r�  (hh(h	}r�  (h*]r�  (}r�  (h-}r�  h�X   secondsr�  s�r�  hX�   Convert a time in seconds since the Epoch to a string in local time.
This is equivalent to asctime(localtime(seconds)). When the time tuple is
not present, current time as returned by localtime() is used.r�  h/]r   hX   strr  �r  au}r  (hM)hW]r  hau}r  (hM}r  (hOX   secondsr  hQ]r  hAau�r	  hW]r
  hauehX�   ctime(seconds) -> string

Convert a time in seconds since the Epoch to a string in local time.
This is equivalent to asctime(localtime(seconds)). When the time tuple is
not present, current time as returned by localtime() is used.r  uuX   gmtimer  }r  (hh(h	}r  (h*]r  (}r  (h-}r  (j�  j�  h�X   secondsr  u�r  hXT  (tm_year, tm_mon, tm_mday, tm_hour, tm_min,
                       tm_sec, tm_wday, tm_yday, tm_isdst)

Convert seconds since the Epoch to a time tuple expressing UTC (a.k.a.
GMT).  When 'seconds' is not passed in, convert the current time instead.

If the platform supports the tm_gmtoff and tm_zone, they are available as
attributes only.r  h/]r  jE  jE  �r  au}r  (hM)hW]r  h�au}r  (hM}r  (hOX   secondsr  hQ]r  hAau�r  hW]r  h�auehXi  gmtime([seconds]) -> (tm_year, tm_mon, tm_mday, tm_hour, tm_min,
                       tm_sec, tm_wday, tm_yday, tm_isdst)

Convert seconds since the Epoch to a time tuple expressing UTC (a.k.a.
GMT).  When 'seconds' is not passed in, convert the current time instead.

If the platform supports the tm_gmtoff and tm_zone, they are available as
attributes only.r  uuX   __spec__r   }r!  (hhh	}r"  h]r#  hX
   ModuleSpecr$  �r%  asuh}r&  (hhh	}r'  (h}r(  (j]  }r)  (hhIh	}r*  (h*NhX>   Initialize self.  See help(type(self)) for accurate signature.r+  uuX	   find_specr,  }r-  (hhh	}r.  h]r/  hX   methodr0  �r1  asuhG}r2  (hhIh	}r3  (h*NhX   Implement delattr(self, name).r4  uujh  }r5  (hh(h	}r6  (h*NhXG   Create and return a new object.  See help(type) for accurate signature.r7  uuX
   is_packager8  }r9  (hhh	}r:  h]r;  j1  asuX   find_moduler<  }r=  (hhh	}r>  h]r?  j1  asuhZ}r@  (hhIh	}rA  (h*NhX   default object formatterrB  uuhh}rC  (hhIh	}rD  (h*NhX   Return hash(self).rE  uuhm}rF  (hhIh	}rG  (h*NhX   Return repr(self).rH  uuh{}rI  (hhIh	}rJ  (h*NhX   Return self<value.rK  uuX   load_modulerL  }rM  (hhh	}rN  h]rO  j1  asuj�  }rP  (hhIh	}rQ  (h*NhX   Return self<=value.rR  uuh�}rS  (hhIh	}rT  (h*NhX   Return self>value.rU  uuX   module_reprrV  }rW  (hh(h	}rX  (h*NhXs   Return repr for the module.

        The method is deprecated.  The import machinery does the job itself.

        rY  uuh�}rZ  (hhIh	}r[  (h*NhX   Return str(self).r\  uuh�}r]  (hhIh	}r^  (h*]r_  }r`  (h-}ra  (h�h�h]rb  h�au�rc  hX   default dir() implementationrd  h/]re  h�auahX.   __dir__() -> list
default dir() implementationrf  uuX
   get_sourcerg  }rh  (hhh	}ri  h]rj  j1  asuh�}rk  (hhIh	}rl  (h*NhX   helper for picklerm  uuh�}rn  (hhh	]ro  h�auh�}rp  (hhIh	}rq  (h*NhX   helper for picklerr  uuj
  }rs  (hhIh	}rt  (h*NhX   Return self==value.ru  uujd  }rv  (hhh	}rw  h]rx  hasuj$  }ry  (hh(h	}rz  (h*NhX4  Abstract classes can override this to customize issubclass().

This is invoked early on by abc.ABCMeta.__subclasscheck__().
It should return True, False or NotImplemented.  If it returns
NotImplemented, the normal algorithm is used.  Otherwise, it
overrides the normal algorithm (and the outcome is cached).
r{  uuX
   __module__r|  }r}  (hhh	}r~  h]r  hasuj�  }r�  (hhIh	}r�  (h*NhX   Return self!=value.r�  uuX   __dict__r�  }r�  (hhh	}r�  h]r�  hX   mappingproxyr�  �r�  asuj�  }r�  (hhIh	}r�  (h*]r�  }r�  (h-}r�  (h�h�h]r�  h�au�r�  hX"   size of object in memory, in bytesr�  h/]r�  j  auahX6   __sizeof__() -> int
size of object in memory, in bytesr�  uuj2  }r�  (hhIh	}r�  (h*NhX%   Implement setattr(self, name, value).r�  uuX   __weakref__r�  }r�  (hh:h	}r�  (hX2   list of weak references to the object (if defined)r�  h]r�  h?auuX   get_coder�  }r�  (hhh	}r�  h]r�  j1  asujC  }r�  (hhIh	}r�  (h*NhX   Return self>=value.r�  uuuhX�   Meta path import for built-in modules.

    All methods are either class or static methods to avoid the need to
    instantiate the class.

    r�  j�  ]r�  (hh?ej�  ]r�  h?aX	   is_hiddenr�  �uuX   asctimer�  }r�  (hh(h	}r�  (h*]r�  (}r�  (h-}r�  (j�  j�  h�X   tupler�  u�r�  hX�   Convert a time tuple to a string, e.g. 'Sat Jun 06 16:26:11 1998'.
When the time tuple is not present, current time as returned by localtime()
is used.r�  h/]r�  j  au}r�  (hM)hW]r�  hau}r�  (hM}r�  (hOX   timer�  hQ]r�  hAau�r�  hW]r�  hauehX�   asctime([tuple]) -> string

Convert a time tuple to a string, e.g. 'Sat Jun 06 16:26:11 1998'.
When the time tuple is not present, current time as returned by localtime()
is used.r�  uuX	   localtimer�  }r�  (hh(h	}r�  (h*]r�  (}r�  (h-}r�  (j�  j�  h�X   secondsr�  u�r�  hX�   (tm_year,tm_mon,tm_mday,tm_hour,tm_min,
                          tm_sec,tm_wday,tm_yday,tm_isdst)

Convert seconds since the Epoch to a time tuple expressing local time.
When 'seconds' is not passed in, convert the current time instead.r�  h/]r�  j  au}r�  (hM)hW]r�  h�au}r�  (hM}r�  (hOX   secondsr�  hQ]r�  hAau�r�  hW]r�  h�auehX  localtime([seconds]) -> (tm_year,tm_mon,tm_mday,tm_hour,tm_min,
                          tm_sec,tm_wday,tm_yday,tm_isdst)

Convert seconds since the Epoch to a time tuple expressing local time.
When 'seconds' is not passed in, convert the current time instead.r�  uuX   altzoner�  }r�  (hhh	}r�  h]r�  (hj�  esuX   daylightr�  }r�  (hhh	}r�  h]r�  (hj�  esuX   strptimer�  }r�  (hh(h	}r�  (h*]r�  (}r�  (h-}r�  h�X   stringr�  s}r�  h�X   formatr�  s�r�  hXb  Parse a string to a time tuple according to a format specification.
See the library reference manual for formatting codes (same as
strftime()).

Commonly used format codes:

%Y  Year with century as a decimal number.
%m  Month as a decimal number [01,12].
%d  Day of the month as a decimal number [01,31].
%H  Hour (24-hour clock) as a decimal number [00,23].
%M  Minute as a decimal number [00,59].
%S  Second as a decimal number [00,61].
%z  Time zone offset from UTC.
%a  Locale's abbreviated weekday name.
%A  Locale's full weekday name.
%b  Locale's abbreviated month name.
%B  Locale's full month name.
%c  Locale's appropriate date and time representation.
%I  Hour (12-hour clock) as a decimal number [01,12].
%p  Locale's equivalent of either AM or PM.

Other codes may be available on your platform.  See documentation for
the C library strftime function.
r�  h/]r�  X   timer�  X   struct_timer�  �r�  au}r�  (hM}r�  (hOX   stringr�  hQ]r�  hau�r�  hW]r�  hAau}r�  (hM}r�  (hOX   stringr�  hQ]r�  hau}r�  (hOX   formatr�  hQ]r�  hau�r�  hW]r�  hAauehX�  strptime(string, format) -> struct_time

Parse a string to a time tuple according to a format specification.
See the library reference manual for formatting codes (same as
strftime()).

Commonly used format codes:

%Y  Year with century as a decimal number.
%m  Month as a decimal number [01,12].
%d  Day of the month as a decimal number [01,31].
%H  Hour (24-hour clock) as a decimal number [00,23].
%M  Minute as a decimal number [00,59].
%S  Second as a decimal number [00,61].
%z  Time zone offset from UTC.
%a  Locale's abbreviated weekday name.
%A  Locale's full weekday name.
%b  Locale's abbreviated month name.
%B  Locale's full month name.
%c  Locale's appropriate date and time representation.
%I  Hour (12-hour clock) as a decimal number [01,12].
%p  Locale's equivalent of either AM or PM.

Other codes may be available on your platform.  See documentation for
the C library strftime function.
r�  uuX   mktimer�  }r�  (hh(h	}r�  (h*]r�  (}r�  (h-}r�  h�X   tupler�  s�r�  hX   Convert a time tuple in local time to seconds since the Epoch.
Note that mktime(gmtime(0)) will not generally return zero for most
time zones; instead the returned value will either be equal to that
of the timezone or altzone attributes on the time module.r�  h/]r�  h2au}r�  (hM}r�  (hOX	   localTimer�  hQ]r   h�au�r  hW]r  hX   floatr  �r  auehX(  mktime(tuple) -> floating point number

Convert a time tuple in local time to seconds since the Epoch.
Note that mktime(gmtime(0)) will not generally return zero for most
time zones; instead the returned value will either be equal to that
of the timezone or altzone attributes on the time module.r  uuX   strftimer  }r  (hh(h	}r  (h*]r	  (}r
  (h-}r  h�X   formatr  s}r  (j�  j�  h�X   tupler  u�r  hX�  Convert a time tuple to a string according to a format specification.
See the library reference manual for formatting codes. When the time tuple
is not present, current time as returned by localtime() is used.

Commonly used format codes:

%Y  Year with century as a decimal number.
%m  Month as a decimal number [01,12].
%d  Day of the month as a decimal number [01,31].
%H  Hour (24-hour clock) as a decimal number [00,23].
%M  Minute as a decimal number [00,59].
%S  Second as a decimal number [00,61].
%z  Time zone offset from UTC.
%a  Locale's abbreviated weekday name.
%A  Locale's full weekday name.
%b  Locale's abbreviated month name.
%B  Locale's full month name.
%c  Locale's appropriate date and time representation.
%I  Hour (12-hour clock) as a decimal number [01,12].
%p  Locale's equivalent of either AM or PM.

Other codes may be available on your platform.  See documentation for
the C library strftime function.
r  h/]r  j  au}r  (hM}r  (hOX   formatr  hQ]r  hau�r  hW]r  hau}r  (hM}r  (hOX   formatr  hQ]r  hau}r  (hOX   dateTimer  hQ]r  h�au�r  hW]r   hauehX�  strftime(format[, tuple]) -> string

Convert a time tuple to a string according to a format specification.
See the library reference manual for formatting codes. When the time tuple
is not present, current time as returned by localtime() is used.

Commonly used format codes:

%Y  Year with century as a decimal number.
%m  Month as a decimal number [01,12].
%d  Day of the month as a decimal number [01,31].
%H  Hour (24-hour clock) as a decimal number [00,23].
%M  Minute as a decimal number [00,59].
%S  Second as a decimal number [00,61].
%z  Time zone offset from UTC.
%a  Locale's abbreviated weekday name.
%A  Locale's full weekday name.
%b  Locale's abbreviated month name.
%B  Locale's full month name.
%c  Locale's appropriate date and time representation.
%I  Hour (12-hour clock) as a decimal number [01,12].
%p  Locale's equivalent of either AM or PM.

Other codes may be available on your platform.  See documentation for
the C library strftime function.
r!  uuX   sleepr"  }r#  (hh(h	}r$  (h*]r%  (}r&  (h-}r'  h�X   secondsr(  s�r)  hXt   Delay execution for a given number of seconds.  The argument may be
a floating point number for subsecond precision.r*  u}r+  (hM}r,  (hOX   tmr-  hQ]r.  j  au�r/  hW]r0  h%auehX�   sleep(seconds)

Delay execution for a given number of seconds.  The argument may be
a floating point number for subsecond precision.r1  uuX   clockr2  }r3  (hh(h	}r4  (h*]r5  (}r6  (h-)hX�   Return the CPU time or real time since the start of the process or since
the first call to clock().  This has as much precision as the system
records.r7  h/]r8  h2au}r9  (hM)hW]r:  j  auehX�   clock() -> floating point number

Return the CPU time or real time since the start of the process or since
the first call to clock().  This has as much precision as the system
records.r;  uuX   timer<  }r=  (hh(h	}r>  (h*]r?  (}r@  (h-)hX{   Return the current time in seconds since the Epoch.
Fractions of a second may be present if the system clock provides them.rA  h/]rB  h2au}rC  (hM)hW]rD  j  auehX�   time() -> floating point number

Return the current time in seconds since the Epoch.
Fractions of a second may be present if the system clock provides them.rE  uuX   get_clock_inforF  }rG  (hh(h	}rH  (h*]rI  }rJ  (h-}rK  h�X   namerL  s�rM  hX'   Get information of the specified clock.rN  h/]rO  hX   dictrP  �rQ  auahXJ   get_clock_info(name: str) -> dict

Get information of the specified clock.rR  uuX   process_timerS  }rT  (hh(h	}rU  (h*]rV  }rW  (h-)hXF   Process time for profiling: sum of the kernel and user-space CPU time.rX  h/]rY  h2auahX_   process_time() -> float

Process time for profiling: sum of the kernel and user-space CPU time.rZ  uujd  }r[  (hhh	}r\  h]r]  (hhesuX   timezoner^  }r_  (hhh	}r`  h]ra  (hj�  esuuu.